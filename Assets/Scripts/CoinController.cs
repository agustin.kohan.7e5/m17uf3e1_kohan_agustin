using UnityEngine;

public class CoinController : MonoBehaviour
{

    private Rigidbody _rb;
    private BoxCollider _boxCollider;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _boxCollider = GetComponent<BoxCollider>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 2f, 0));        
    }

    private void OnCollisionEnter(Collision collision)
    {
        _rb.isKinematic = true;
        _boxCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.CoinsEarned++;
            UIManager.Instance.UpdateCoinsCounter();
            Destroy(gameObject);
        }
    }
}
