using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    public enum GameState
    {
        Playing,
        Pause,
        Finish
    }

    public GameState gameState;
    public int CoinsToSpawn;
    public int CoinsEarned;

    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        CoinsEarned = 0;
        SingletonInstance();
    }

    void SingletonInstance()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (CoinsEarned == CoinsToSpawn)
        {
            UIManager.Instance.ResetButton.gameObject.SetActive(true);
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
