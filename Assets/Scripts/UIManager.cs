using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager instance = null;
    public static UIManager Instance { get { return instance; } }

    public TextMeshProUGUI TimerText;
    public TextMeshProUGUI CoinsCounter;
    public Button ResetButton; 
    // Start is called before the first frame update
    void Start()
    {
        SingletonInstance();
        UpdateCoinsCounter();
    }

    void SingletonInstance()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        TimerText.text = FormatSeconds();
    }

    public void UpdateCoinsCounter()
    {
        if (GameManager.Instance.CoinsEarned == GameManager.Instance.CoinsToSpawn)
            CoinsCounter.color = Color.green;
        else if (GameManager.Instance.CoinsEarned <= GameManager.Instance.CoinsToSpawn / 2)
            CoinsCounter.color = Color.red;
        else if (GameManager.Instance.CoinsEarned >= GameManager.Instance.CoinsToSpawn / 2)
            CoinsCounter.color = Color.yellow;
        
        
        CoinsCounter.text = GameManager.Instance.CoinsEarned + "/" + GameManager.Instance.CoinsToSpawn;
    }

    string FormatSeconds()
    {
        int d = (int)(Time.timeSinceLevelLoad * 100.0f);
        int minutes = d / (60 * 100);
        int seconds = (d % (60 * 100)) / 100;
        int hundredths = d % 100;
        if (seconds == 0)
            TimerText.color = Color.red;
        else
            TimerText.color = Color.white;
        return String.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, hundredths);
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(0);
    }
}
