using UnityEngine;

public class CoinSpawnerController : MonoBehaviour
{
    [SerializeField]
    GameObject _coinPrefab;
    public MeshRenderer SpawnSurface;
    // Start is called before the first frame update
    void Start()
    {
        SpawnCoins();
    }

    private void SpawnCoins()
    {
        for (int i = 0; i < GameManager.Instance.CoinsToSpawn; i++)
        {
            transform.position = new Vector3(Random.Range(SpawnSurface.bounds.min.x, SpawnSurface.bounds.max.x), 10f, Random.Range(SpawnSurface.bounds.min.z, SpawnSurface.bounds.max.z));
            Instantiate(_coinPrefab, transform.position, Quaternion.identity);
        }
    }
}
